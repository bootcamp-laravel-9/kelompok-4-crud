<?php

namespace App\Http\Controllers;

use App\Models\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Spatie\FlareClient\View;

class AuthController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function login()
    {
        return View('auth.login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function register()
    {
        return View('auth.register');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function loginProccess(Request $request)
    {
        $user = Profile::where('email', $request->email)->first();

        if (!$user) {
            return back()->with('error', 'Email atau Password salah!');
        }

        $isValidPassword = Hash::check($request->password, $user->password);

        // $isValidPassword = $request->password === $user->password;

        if (!$isValidPassword) {
            return back()->with('error', 'Email atau Password salah!');
        }

        $token = $user->createToken(config('app.name'))->plainTextToken;
        $request->session()->put('LoginSession', $token);
        $request->session()->put('user_id', $user->id);
        $request->session()->put('user_nama', $user->nama);
        $request->session()->put('user_foto', $user->url_foto);
        return redirect('/');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function registerProccess(Request $request)
    {
        $validated = $request->validate([
            'nama' => ['required', 'string', 'max:24', 'regex:/^[^\W\d_]+$/'],
            'email' => ['required', 'email', 'unique:profiles', 'max:100', 'regex:/^\S+$/'],
            'username' => ['required', 'string', 'max:25'],
            'alamat' => ['required', 'string', 'max:50'],
            'password' => ['required', 'min:8', 'max:15'],
            'confirmPassword' => ['same:password'],
        ]);

        $user = Profile::create([
            'nama' => $request->nama,
            'email' => $request->email,
            'username' => $request->username,
            'alamat' => $request->alamat,
            'password' => Hash::make($request['password']),
        ]);
        return redirect('/login');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $request->session()->flush();
        return redirect('/login');
    }
}
