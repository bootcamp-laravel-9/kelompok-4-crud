<?php

namespace App\Http\Controllers;

use App\Models\Profile;
use App\Http\Requests\StoreProfileRequest;
use App\Http\Requests\UpdateProfileRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Mengambil ID pengguna dari sesi
        $user_id = $request->session()->get('user_id');

        // Mengambil data pengguna dari database berdasarkan ID
        $user = Profile::find($user_id);

        // Mengirim data pengguna ke tampilan
        return view('profile.index', ['user' => $user, 'isEdit' => false]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreProfileRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProfileRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function show(Profile $profile)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $user_id = $request->session()->get('user_id');

        // Mengambil data pengguna dari database berdasarkan ID
        $user = Profile::find($user_id);

        // Mengirim data pengguna ke tampilan
        return view('profile.index', ['user' => $user, 'isEdit' => true]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateProfileRequest  $request
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validated = $request->validate([
            'nama' => ['string', 'max:24',],
            'email' => ['email', 'max:100'],
            'username' => ['string', 'max:25'],
            'alamat' => ['filled', 'string', 'max:50'],
            'url_foto' => ['image', 'file', 'max:1024'],
        ]);

        $updateProfile = Profile::find($request->id);
        $updateProfile->nama = $request->nama;
        $updateProfile->email = $request->email;
        $updateProfile->username = $request->username;
        $updateProfile->alamat = $request->alamat;

        $imageUpload = $request->file('url_foto');
        if ($imageUpload) {
            $imageUpload->move(public_path('images'), $imageUpload->getClientOriginalName());
            $updateProfile->url_foto = $imageUpload->getClientOriginalName();
        }

        if ($updateProfile->save()) {
            Session::put('user_foto', $updateProfile->url_foto);

            return redirect('/profile')->with('success', 'Data berhasil diubah!');
        } else {
            return redirect()->back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profile $profile)
    {
        //
    }
}
