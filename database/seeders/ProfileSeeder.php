<?php

namespace Database\Seeders;

use App\Models\Profile;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $profiles = [
            [
                'nama' => 'Ryzal',
                'email' => 'ryzal@mail.com',
                'username' => 'ryzalaja',
                'alamat' => 'Sumedang',
                'password' => 'admin123',
                'url_foto' => 'img/ryzal.jpg',

            ],
            [
                'nama' => 'Samuel',
                'email' => 'samuel@mail.com',
                'username' => 'samuel',
                'alamat' => 'Purworejo',
                'password' => 'admin123',
                'url_foto' => 'img/samuel.jpg',

            ],
        ];

        DB::beginTransaction();

        foreach ($profiles as $profile) {
            Profile::firstOrCreate($profile);
        }

        DB::commit();
    }
}
