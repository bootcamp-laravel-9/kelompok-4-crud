@extends('layout/main')
@section('menu-profile', 'active')
@section('item-route', 'Profile')
@section('menu-title', 'Profile')

@section('content')
    <div class="card card-primary">
        @if (session('success'))
            <div id="success-alert" class="alert alert-success" style="background-color: #007F73;">
                {{ session('success') }}
            </div>
        @endif

        <!-- form start -->
        <form method="POST" action="{{ url('/profile-edit-proces') }}" enctype="multipart/form-data">
            @csrf
            <div class="card-body">
                <div class="d-flex  justify-content-center mb-5">
                    <img src="{{ $user->url_foto ? 'images/' . $user->url_foto : 'http://via.placeholder.com/360x360' }}"
                        class="rounded-circle" width=180 height=180 alt="avatar">
                </div>
                <div class="form-group d-flex align-items-center">
                    <label for="id" class="flex-shrink-0 w-25">ID</label>
                    <input disabled type="text" class="form-control" name="id" id="id"
                        value="{{ $user->id }}">
                    <input type="hidden" name="id" id="id" value="{{ $user->id }}">
                </div>
                <div class="form-group d-flex align-items-center">
                    <label for="nama" class="flex-shrink-0 w-25">Nama</label>
                    <input @disabled(!$isEdit) type="text" class="form-control flex-grow-1" name="nama"
                        id="nama" value="{{ $user->nama }}">
                </div>
                <div class="form-group d-flex align-items-center">
                    <label for="username" class="flex-shrink-0 w-25">Username</label>
                    <input @disabled(!$isEdit) type="text" class="form-control flex-grow-1" name="username"
                        id="username" value="{{ $user->username }}">
                </div>
                <div class="form-group d-flex align-items-center">
                    <label for="email" class="flex-shrink-0 w-25">Email</label>
                    <input @disabled(!$isEdit) type="email" class="form-control flex-grow-1" name="email"
                        id="email" value="{{ $user->email }}">
                </div>
                <div class="form-group d-flex align-items-center">
                    <label for="alamat" class="flex-shrink-0 w-25">Alamat</label>
                    <input @disabled(!$isEdit) type="text" class="form-control flex-grow-1" name="alamat"
                        id="alamat" value="{{ $user->alamat }}">
                </div>
                @if ($isEdit)
                    <div class="form-group d-flex align-items-center">
                        <label for="url_foto" class="flex-shrink-0 w-25">Url Foto</label>
                        <input type="file" class="form-control" id="url_foto" name="url_foto">
                    </div>
                @endif
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                @if ($isEdit)
                    <a href="{{ url('/profile') }}" class="btn btn-danger">Cancel</a>
                    <button type="submit" class="btn btn-primary">Save</button>
                @else
                    <a href="{{ url('/profile-edit') }}" class="btn btn-success">Edit</a>
                @endif
            </div>
        </form>
    </div>
@endsection
