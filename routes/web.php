<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => ['isNotLogin']], function () use ($router) {
    Route::get('/', function () {
        return view('dashboard.index');
    });
    Route::get('/profile', 'ProfileController@index');
    Route::get('/profile-edit', 'ProfileController@edit');
    Route::post('/profile-edit-proces', 'ProfileController@update');
    Route::get('/logout', 'AuthController@logout');
});

Route::group(['middleware' => ['isLogin']], function () use ($router) {
    Route::get('/login', 'AuthController@login')->name('login');
    Route::post('/login', 'AuthController@loginProccess');
    Route::get('/register', 'AuthController@register')->name('register');
    Route::post('/register', 'AuthController@registerProccess')->name('register');
});
